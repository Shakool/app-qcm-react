import React, { Component } from "react";
import Question from "./Question";

class Qcm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
  }
  componentDidMount() {
    fetch("/data/questions.json")
      .then((response) => response.json())
      .then((data) =>
        setTimeout(() => {
          this.setState({ data, isLoading: false });
        }, 1000)
      );
  }
  render() {
    const loader = this.state.isLoading ? (
      <span>Loading...</span>
    ) : (
      this.state.data.questions.map((q) => {
        const { id, title, question, c1, c2, response, difficulty } = q;
        return (
          <Question
            id={id}
            key={id}
            title={title}
            question={question}
            c1={c1}
            c2={c2}
            response={response}
            difficulty={difficulty}
          ></Question>
        );
      })
    );
    return (
      <>
        <h2> QCM </h2>
        {loader}
      </>
    );
  }
}

export default Qcm;
